import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Animated, {
  divide,
  interpolate,
  Extrapolate,
  sub,
  cond,
  add,
  lessThan,
  multiply,
} from "react-native-reanimated";
import { HEIGHT } from "./ItemLayout";

const styles = StyleSheet.create({
  remove: {
    color: "white",
    fontFamily: "UberMoveMedium",
    fontSize: 14,
  },
});

interface ActionProps {
  x: Animated.Node<number>;
  deleteOpacity: Animated.Node<number>
}

const Action = ({ x, deleteOpacity }: ActionProps) => {
  const circleSize = cond(lessThan(x, HEIGHT), x, add(x, sub(x, HEIGHT)));
  const translateX = cond(lessThan(x, HEIGHT), 0, divide(sub(x, HEIGHT), 2));
  const borderRadius = divide(circleSize, 2);
  const iconOpacity = interpolate(circleSize, {
    inputRange: [HEIGHT - 10, HEIGHT + 10],
    outputRange: [1, 0],
  });
  const textOpacity = interpolate(circleSize, {
    inputRange: [HEIGHT - 10, HEIGHT + 10],
    outputRange: [0, 1],
  });
  const iconScale = interpolate(circleSize, {
    inputRange: [20, 30],
    outputRange: [0.001, 1],
    extrapolate: Extrapolate.CLAMP,
  });

  return (
    <Animated.View
      style={{
        backgroundColor: "#D93F12",
        justifyContent: "center",
        alignItems: "center",
        height: circleSize,
        width: circleSize,
        borderRadius,
        transform: [{ translateX }],
      }}
    >
      <Animated.View
        style={{
          height: 3,
          width: 15,
          backgroundColor: "white",
          opacity: iconOpacity,
          transform: [{ scale: iconScale }],
        }}
      />
      <Animated.View
        style={{
          ...StyleSheet.absoluteFillObject,
          justifyContent: "center",
          alignItems: "center",
          opacity: multiply(textOpacity, deleteOpacity),
        }}
      >
        <Text style={styles.remove}>Remove</Text>
      </Animated.View>
    </Animated.View>
  );
};

export default Action;
